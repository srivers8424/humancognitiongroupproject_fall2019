Pipeline:

- Each dev works in their own respective dev scene in order to avoid merge conflicts.
- Expecially with Unity's new prefabs, each person when working on a feature of high importance should make a prefab of such things for easy use and integration.


Source Control Guidelines:


Before you start working on the project:
- Pull from the development branch in order to make sure you're up to date with us other developers

After each major feature, or when you are finished and don't plan on working again within an hour:


- Pull from the development branch in order to make sure you're up to date with us other developers
- When ready, open SourceTree and press commit.
    *When doing this, make sure you are on the development branch that you checked out in the beginning.
- Stage all of your files, and write a commit message in the bottom explaining what changed and why.
    *Be descriptive, yet light. 
- Press commit
- You are going to push after each commit
    *Ensure you're pushing to the remote development branch(not master).
    
For more information on using SourceTree with Unity Projects, visit:

https://unity3d.com/fr/learn/tutorials/topics/cloud-build/creating-your-first-source-control-repository?_ga=2.30934399.1162476207.1571152695-40654131.1562617447