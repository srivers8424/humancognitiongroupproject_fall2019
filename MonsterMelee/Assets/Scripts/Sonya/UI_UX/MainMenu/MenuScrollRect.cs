﻿/*  @author Sonya Rivers-Medina
 *  @date 12/2/2019
 *  @purpose Helper to control the Shop Menu found in the Main Menu
 */
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using DG.Tweening;

namespace SonyaScripts
{
    public class MenuScrollRect : MonoBehaviour
    {
        private ScrollRect scrollRect;
        private bool mouseDown, buttonLeft, buttonRight;

        private void Start()
        {
            scrollRect = GetComponent<ScrollRect>();
        }

        private void Update()
        {
            if(mouseDown)
            {
                if(buttonLeft)
                {
                    ScrollLeft();
                }
                else if(buttonRight)
                {
                    ScrollRight();
                }
            }
        }

        public void ButtonLeftIsPressed()
        {
            mouseDown = true;
            buttonLeft = true;
        }

        public void ButtonRightIsPressed()
        {
            mouseDown = true;
            buttonRight = true;
        }

        public void ScrollLeft()
        {
            if(Input.GetMouseButtonUp(0))
            {
                mouseDown = false;
                buttonLeft = false;
            }
            else
            {
                scrollRect.horizontalNormalizedPosition -= 0.1f;
            }
        }

        public void ScrollRight()
        {
            if (Input.GetMouseButtonUp(0))
            {
                mouseDown = false;
                buttonRight = false;
            }
            else
            {
                scrollRect.horizontalNormalizedPosition += 0.1f;
            }
        }
    }
}

