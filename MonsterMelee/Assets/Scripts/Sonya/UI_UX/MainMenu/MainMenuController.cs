﻿/*  @author Sonya Rivers-Medina
 *  @date 11/20/2019
 *  @purpose Manages the Main Menu
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SonyaScripts
{
    public class MainMenuController : MonoBehaviour
    {
        public void StartButtonPressed()
        {
            LevelManager.instance.LoadNext();
        }

        public void StoreButtonPressed()
        {

        }

        public void InboxButtonPressed()
        {

        }

        public void AchievementsButtonPressed()
        {

        }

        public void SettingsButtonPressed()
        {

        }
    }

}