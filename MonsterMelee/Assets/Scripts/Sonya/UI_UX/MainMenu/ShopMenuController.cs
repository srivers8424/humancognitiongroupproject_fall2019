﻿/*  @author Sonya Rivers-Medina
 *  @date 11/21/2019
 *  @purpose Controls the Shop Menu found in the Main Menu
 */
using UnityEngine;
using UnityEngine.UI;

namespace SonyaScripts
{
    /// <summary>
    /// Controls the Shop Menu found in the Main Menu
    /// </summary>
    public class ShopMenuController : MonoBehaviour
    {
        public ScrollRect scrollRect;

        public void PreviousItem()
        {
            scrollRect.horizontalNormalizedPosition -= 0.1f;
        }

        public void NextItem()
        {
            scrollRect.horizontalNormalizedPosition += 0.1f;
        }

    }

}
