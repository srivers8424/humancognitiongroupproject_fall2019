﻿/*  @author Sonya Rivers-Medina
 *  @date 11/21/2019
 *  @purpose Helper to control the Shop Menu found in the Main Menu
 */
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using DG.Tweening;

namespace SonyaScripts
{
    public class ShopMenuScrollButton : MonoBehaviour
    {
        public MenuScrollRect scroll;
        public bool scrollLeft;
       
        public void Scroll()
        {
            if(scrollLeft)
            {
                scroll.ButtonLeftIsPressed();
            }
            else
            {
                scroll.ButtonRightIsPressed();
            }
        }

    }
}
