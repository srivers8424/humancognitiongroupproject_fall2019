﻿/*  @author Sonya Rivers-Medina
 *  @date 11/14/2019
 *  @purpose Fades black screen in/out between scene transitions
 */
 using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace SonyaScripts
{
    public class Fade : MonoBehaviour
    {
        private Image img;

        private void Start()
        {
            img = GetComponent<Image>();
        }
        
        public void Show()
        {
            img.DOFade(1.0f, 0.35f);
        }

        
        public void Hide()
        {
            img.DOFade(0.0f, 0.55f);
        }


    }

}
