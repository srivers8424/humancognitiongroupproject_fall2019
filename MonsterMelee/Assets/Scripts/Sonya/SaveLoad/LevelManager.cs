﻿/*  @author Sonya Rivers-Medina
*  @date 2/26/2019
*  @purpose Transitions between scenes with asynchronus loading
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using DG.Tweening;

namespace SonyaScripts
{
    public class LevelManager : MonoBehaviour
    {
        
        public static LevelManager instance;
        [SerializeField] GameObject fadePanel;
       // [SerializeField] TMP_Text randomFact;
       // private Color initRandomFactColor;
       // [SerializeField] Color randomFactColor;
        AsyncOperation async;
        public DataService data;
        public GameObject slotMachineGroup;
       // [SerializeField] List<string> RandomFacts;
        bool loadingRegion; //Whether the scene is loading a region or smaller scene
        bool waitForLoad; //Keeps track of loading screen
        float timer;
        float timerWait; //Minimum wait time to load

        private void Awake()
        {
            data = new DataService("MonsterMelee.db");
            //data.CreateFactDatabase(); //TODO: Create a database of random facts

            //Create a singleton instance of level manager to be used in all scenes
            if (instance == null)
            {
                instance = this;
            }

        }

        private void Start()
        {
            waitForLoad = false;
            timerWait = 5.0f; //Changed value from 10 to 5  @DS
          //  initRandomFactColor = randomFact.color;
        }

     
        private void Update()
        {
            if (waitForLoad)
            {
                timer += Time.deltaTime;
                //If you aren't loading a region load according to async load is 90% complete
                if (!loadingRegion)
                {
                    if (timer >= timerWait) //&& async.progress >= 0.9f)
                    {
                        waitForLoad = false;
                        async.allowSceneActivation = true;
                        Show();
                    }
                }
                //If you ARE loading a region then wait min 10 seconds and display fact
                else
                {
                    if (timer >= timerWait)
                    {
                        waitForLoad = false;
                        async.allowSceneActivation = true;
                        Show();
                    }
                }

            }

            if(Input.GetKeyDown(KeyCode.O))
            {
                slotMachineGroup.SetActive(true);
            }
        }

        public void LoadMainMenu()
        {
            //If you're not already in the Main Menu
            if(SceneManager.GetActiveScene().name != "MainMenu")
            {
                LoadSceneByName("MainMenu");
            }
            
        }

        public void LoadNext()
        {
            waitForLoad = true;
            loadingRegion = false;
            timerWait = 0.1f;
            Hide();
            async = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
            async.allowSceneActivation = false;
            timer = 0;
        }

        //Loads a scene by its name
        public void LoadSceneByName(string sceneName)
        {
            waitForLoad = true;
            loadingRegion = false;
            timerWait = 0.1f;
            Hide();
            //   async = SceneManager.LoadSceneAsync(sceneName);
            // async.allowSceneActivation = false;
            SceneManager.LoadSceneAsync(sceneName);
            timer = 0;
        }

        /// <summary>
        /// Similar to LoadSceneByName but uses longer load time and shows an image
        /// and a fun fact about the game's lore or a gameplay hint
        /// </summary>
        /// <param name="regionSceneName"></param>
        public void LoadRegionByName(string regionSceneName)
        {
            waitForLoad = true;
            loadingRegion = true;
            Hide();
            DisplayRandomFactOnLoad();
            async = SceneManager.LoadSceneAsync(regionSceneName);
            async.allowSceneActivation = false;
            timer = 0;
        }

        private void Show()
        {
            //GetComponent<Animator>().SetBool("show", true);
            //  GetComponent<Animator>().SetBool("loadingRegion", loadingRegion);
            //  fadePanel.GetComponent<Animator>().SetBool("show", true);
            fadePanel.GetComponent<Fade>().Show();
            //ResetRandomFact();
        }

        private void Hide()
        {
            // GetComponent<Animator>().SetBool("show", false);
            //  GetComponent<Animator>().SetBool("loadingRegion", loadingRegion);
            fadePanel.GetComponent<Fade>().Hide();
         //   fadePanel.GetComponent<Animator>().SetBool("show", false);
        }

        private void DisplayRandomFactOnLoad()
        {
            //randomFact.text = RandomFacts[Random.Range(0, RandomFacts.Count)];
            StartCoroutine(FadeFactColor());
        }

        private IEnumerator FadeFactColor()
        {
            yield return new WaitForSeconds(1.0f);
           // randomFact.DOColor(randomFactColor, 2.0f);//(255f, 1.0f);
        }

        private void ResetRandomFact()
        {
          //  randomFact.DOColor(initRandomFactColor, 0.5f);
        }
     
        public void QuitGame()
        {
            Application.Quit();
        }
    }

}