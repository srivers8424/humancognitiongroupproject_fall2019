﻿/*  @author Sonya Rivers-Medina
 *  @date 2/26/2019
 *  @purpose Manipulates the configuration systems for video components
 */
using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace SonyaScripts
{
    public class Video_Config : MonoBehaviour
    {
        #region Private Member Variables
        private DataService data;
        [SerializeField] private TextMeshProUGUI resolutionText;
        [SerializeField] private TextMeshProUGUI fullscreenText;
        [SerializeField] private Slider antialiasingSlider;
        #endregion

        #region Public Member Variables
        public static PlayerSettings playerSettings;
        #endregion


        private void Start()
        {
            //Create a connection to the database and a table reference for the PlayerSettings
            data = new DataService("ArcLightDB.db");
            data.CreatePlayerSettingsDatabase();
            playerSettings = new PlayerSettings();
            playerSettings.ID = 1;
            LoadVideoSettings();
        }

        public void SetDefaults()
        {
            if (AudioManager.instance != null)
            {
                AudioManager.instance.SetDefaults();
            }
            resolutionText.text = "HIGH";
            SetQuality();
            fullscreenText.text = "ON";
            ToggleFullScreen();
            antialiasingSlider.value = 4;
            SetAA();
        }

        /// <summary>
        ///  Toggles fullscreen on/off
        /// </summary>
        public void ToggleFullScreen()
        {
            if (fullscreenText.text == "ON")
            {
                Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
            }
            else
            {
                Screen.fullScreenMode = FullScreenMode.Windowed;
            }
        }

        /// <summary>
        ///  Sets anti-aliasing by a slider value from 0-8
        /// </summary>
        public void SetAA()
        {
            int Samples = Convert.ToInt32(antialiasingSlider.value);

            if (Samples == 0 || Samples == 2 || Samples == 4 || Samples == 8)
            {
                QualitySettings.antiAliasing = Samples;
            }

        }

        /// <summary>
        ///  Sets the overall quality settings (low, medium, high, or ultra)
        /// </summary>
        public void SetQuality()
        {
            switch (resolutionText.text)
            {
                case "LOW":
                    QualitySettings.SetQualityLevel(1);
                    break;
                case "MEDIUM":
                    QualitySettings.SetQualityLevel(2);
                    break;
                case "HIGH":
                    QualitySettings.SetQualityLevel(3);
                    break;
                case "ULTRA":
                    QualitySettings.SetQualityLevel(4);
                    break;
            }
        }

        /// <summary>
        /// Saves Video Configuration Settings to the database's Player Settings
        /// </summary>
        public void SaveVideoSettings()
        {
            if (AudioManager.instance != null)
            {
                AudioManager.instance.SaveAudioSettings();
            }

            //Assigns the data from the UI text fields to the PlayerSettings data
            //then saves to the local database
            playerSettings.Resolution = resolutionText.text;
            playerSettings.Fullscreen = fullscreenText.text;
            data.UpdatePlayerSettings();
        }

        private void LoadPlayerSettings()
        {
            playerSettings = data.LoadPlayerSettings();
        }

        /// <summary>
        /// Loads Video Configuration Settings from the database's Player Settings
        /// </summary>
        public void LoadVideoSettings()
        {
            LoadPlayerSettings();

            if (AudioManager.instance != null)
            {
                AudioManager.instance.LoadAudioSettings();
            }

            SetQuality();
            ToggleFullScreen();
            SetAA();
            resolutionText.text = playerSettings.Resolution;
            fullscreenText.text = playerSettings.Fullscreen;
        }

    }


}
