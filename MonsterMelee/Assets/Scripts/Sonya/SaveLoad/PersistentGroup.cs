﻿/*  @author Sonya Rivers-Medina
 *  @date 11/20/2019
 *  @purpose Allows the persistent prefab components to remain in every scene
 *  with DontDestroyOnLoad for all of this parent object's children
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SonyaScripts
{
    /// <summary>
    /// Allows the persistent prefab components to remain in every scene
    /// with DontDestroyOnLoad for all of this parent object's children
    /// </summary>
    public class PersistentGroup : MonoBehaviour
    {
        public bool debugGroup = true;
        private PersistentGroup instance;

        private void Awake()
        {
            if(!debugGroup)
            {
                if (instance == null)
                {
                    instance = this;
                }
            }
            else
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(this);
        }
    }
}


