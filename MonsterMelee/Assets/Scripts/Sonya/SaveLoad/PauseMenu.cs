﻿/*  @author Sonya Rivers-Medina
 *  @date 11/14/2019
 *  @purpose Creates pop-up for Pause Menu UI and pauses gameplay in real-time
 */
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace SonyaScripts
{
    public class PauseMenu : MonoBehaviour
    {
        private Canvas canvas;
        private bool gamePaused = false;
        private string currentScene = "";

        private void Awake()
        {
            canvas = GetComponent<Canvas>();
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += SetSceneName;
        }

        private void OnDisable()
        {
            SceneManager.sceneLoaded -= SetSceneName;
        }

        private void Update()
        {
            if (Input.GetButtonDown("Cancel"))
            {
                if (!gamePaused && currentScene != "MainMenu")
                {
                    PauseGame();
                }
                else
                {
                    PlayGame();
                }
            }
        }
        public void PauseGame()
        {
            Time.timeScale = 0;
            gamePaused = true;
            canvas.enabled = true;
        }

        public void PlayGame()
        {
            canvas.enabled = false;
            gamePaused = false;
            Time.timeScale = 1;
        }

        public void Restart()
        {
            Debug.Log("Restarting Game From Pause Menu...");
        }

        public void SaveGame()
        {
            Debug.Log("Saving Game Data From Pause Menu...");
        }

        public void LoadMainMenu()
        {
            if (LevelManager.instance != null)
            {
                LevelManager.instance.LoadMainMenu();
            }
        }

        
        public void QuitGame()
        {
            if (LevelManager.instance != null)
            {
                LevelManager.instance.QuitGame();
            }
        }

        private void SetSceneName(Scene scene, LoadSceneMode loadSceneMode)
        {
            currentScene = scene.name;
            if(currentScene == "MainMenu")
            {
                canvas.enabled = false;
            }
        }
    }
}
