﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SonyaScripts
{
    
    /// <summary>
    /// Base enemy class to be used by all inheriting enemy subclasses
    /// </summary>
    public class Enemy : MonoBehaviour
    {
        public HealthSystem EnemyHealth;

        private void Start()
        {
            PlayerAnimManager.instance.OnPlayerSlashAttack += TakeDamage;
        }

        private void TakeDamage()
        {
        }
      
    }

}
