﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SonyaScripts
{
    /// <summary>
    /// Base class for any object that can take/deal damage to their Health System
    /// </summary>
    public class HealthSystem : MonoBehaviour
    {
        //Health Stats
        public int MaxHealth = 100;
        public int CurrentHealth;
        public int ArmorValue;
        private int AttackValue;

        //Health Audio

        //Health UI

        //Health Events
    }
}


