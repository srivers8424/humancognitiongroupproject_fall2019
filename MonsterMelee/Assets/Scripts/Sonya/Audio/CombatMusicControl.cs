﻿/*  @author Sonya Rivers-Medina
 *  @date 11/14/2019
 *  @purpose  Sets combat music for audio mixer
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

namespace SonyaScripts
{
   public class CombatMusicControl : MonoBehaviour
    {

        [SerializeField] private AudioMixerSnapshot outOfCombat;
        [SerializeField] private AudioMixerSnapshot inCombat;
        [SerializeField] private AudioClip[] stings;
        private AudioSource stingSource;

        public float bpm = 128; //measures the tempo to optimize fade

        private float m_TransitionIn; //times in milliseconds to fade music transitions from quiet to action snapshots
        private float m_TransitionOut;
        private float m_QuarterNote;


        private void Start()
        {
            if (AudioManager.instance != null)
            {
                stingSource = AudioManager.instance.stingMusic;
            }
            m_QuarterNote = 60 / bpm; //A quarter of the beats per minute in milliseconds
            m_TransitionIn = m_QuarterNote * 0.8f; //fade in quickly over the length of one quarter note
            m_TransitionOut = m_QuarterNote * 16; // 8 bars to fade out slowly
        }


        //Transition into high intensity combat audio when colliding with in-game trigger
        //private void OnTriggerEnter(Collider other)
        //{
        //    if (other.CompareTag("Player"))
        //    {
        //        inCombat.TransitionTo(m_TransitionIn);
        //        PlaySting();
        //    }
        //}

        ////Transition out of combat audio with in-game trigger
        //private void OnTriggerExit(Collider other)
        //{
        //    if (other.CompareTag("AudioTrigger"))
        //    {
        //        outOfCombat.TransitionTo(m_TransitionOut);
        //    }
        //}

        /// <summary>
        /// Transition into high intensity combat audio when InCombat event is triggered
        /// </summary>
        public void TriggerCombatMusic()
        {
            inCombat.TransitionTo(m_TransitionIn);
            PlaySting();
            AudioManager.instance.SetMainMusicVolume(-80f);
        }

        /// <summary>
        /// Transition out of combat audio when ExitCombat event is triggered
        /// </summary>
        public void StopCombatMusic()
        {
            outOfCombat.TransitionTo(m_TransitionOut);
            AudioManager.instance.SetMainMusicVolume(-20f);

        }

        /// <summary>
        /// Generates a random index from array of sounds to assign to stings audio source
        /// </summary>
        private void PlaySting()
        {
            int randClip = Random.Range(0, stings.Length);
            stingSource.clip = stings[randClip];
            stingSource.Play();
        }
    }

}