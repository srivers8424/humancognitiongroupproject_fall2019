﻿/*  @author Sonya Rivers-Medina
 *  @date 4/9/2019
 *  @purpose  Display that allows each scene to have its own music
 *            then sends it to the preconfiugred Audio Controller
 */

using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace SonyaScripts
{
    public class SceneAudioDisplay : MonoBehaviour
    {
        public GameSceneAudio sceneAudio;
        public static SceneAudioDisplay instance;
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        private void Start()
        {
            SetSceneAudio();
        }

        public void SetSceneAudio()
        {
            //Make sure the volume level is correct for each scene
            if (sceneAudio.mainSceneMusicVolume != AudioManager.instance.mainMusicVolume)
            {
                AudioManager.instance.SetMainMusicVolume(sceneAudio.mainSceneMusicVolume);
            }
            if (sceneAudio.ambientSoundVolume != AudioManager.instance.ambienceVolume)
            {
                AudioManager.instance.SetAmbientVolume(sceneAudio.ambientSoundVolume);
            }

            AudioManager.instance.mainMusic.clip = sceneAudio.mainSceneMusic[GetMainSceneMusicRange()];
            AudioManager.instance.stingMusic.clip = sceneAudio.stingSceneMusic;
            AudioManager.instance.ambientSound.clip = sceneAudio.ambientSound;


            AudioManager.instance.mainMusic.Play();
            //AudioManager.instance.stingMusic.Play(); //No need to play the stings on Start()
            AudioManager.instance.ambientSound.Play();
        }

        public IEnumerator SetSceneAudioOnDelay()
        {
            yield return new WaitForSeconds(1.0f);
            SetSceneAudio();
        }
        public int GetMainSceneMusicRange()
        {
            return (int)Random.Range(0.0f, (float)sceneAudio.mainSceneMusic.Count);
        }

        /// <summary>
        /// Plays a sting one-shot for special effect
        /// </summary>
        public void PlayStingOneShot()
        {
            AudioManager.instance.stingMusic.PlayOneShot(sceneAudio.stingSceneMusic);
        }

        /// <summary>
        /// Plays a sting one-shot for special effect
        /// </summary>
        public void PlayStingOneShot(AudioClip sting)
        {
            AudioManager.instance.stingMusic.PlayOneShot(sting);
        }

        /// <summary>
        /// Adjusts the scene's main music volume when the scene loads in
        /// </summary>
        private void SetMainSceneMusicVolume()
        {

        }

        /// <summary>
        /// Adjusts the scene's ambience volume when the scene loads in
        /// </summary>
        private void SetAmbienceVolume()
        {

        }

        /// <summary>
        /// Resets the audio source clip after another clip has finished playing
        /// </summary>
        /// <param name="source"></param>
        /// <param name="clip"></param>
        private IEnumerator ResetAudioClip(AudioSource source, AudioClip clip)
        {
            //Wait until the current clip is finished
            yield return new WaitUntil(() => !source.isPlaying);
            source.clip = clip;

        }


    }

}