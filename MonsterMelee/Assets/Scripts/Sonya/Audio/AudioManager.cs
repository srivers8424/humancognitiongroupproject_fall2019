﻿/*  @author Sonya Rivers-Medina
 *  @date 4/2/2019
 *  @purpose Manages the audio system of the game
 */

using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace SonyaScripts
{
    public class AudioManager : MonoBehaviour
    {
        #region Private Class Member Variables
        [SerializeField] private AudioMixer mixer = null;
        [SerializeField] private Slider masterSlider = null;
        [SerializeField] private Slider mainMusicSlider = null;
        [SerializeField] private Slider sfxSlider = null;
        [SerializeField] private Slider ambienceSlider = null;
        #endregion

        #region Public Class Member Variables
        public static AudioManager instance;
        public AudioSource mainMusic;
        public AudioSource stingMusic;
        public AudioSource ambientSound;
        public AudioSource sfxSound;

        public float mainMusicVolume;
        public float ambienceVolume;
        #endregion

        /// <summary>
        /// Initializes singleton instance
        /// </summary>
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        public void GetDefaultVolumeLevels()
        {
            mixer.GetFloat("MainMusicVolume", out mainMusicVolume);
            mixer.GetFloat("AmbienceVolume", out ambienceVolume);
        }

        /// <summary>
        /// //Resets to default settings
        /// </summary>
        public void SetDefaults()
        {
            mixer.SetFloat("MasterVolume", 0f);
            mixer.SetFloat("MainMusicVolume", -20f);
            mixer.SetFloat("AmbienceVolume", -10f);
            mixer.SetFloat("SFXVolume", -30f);

            if (masterSlider != null)
            {
                masterSlider.value = 0f;
            }

            if (mainMusicSlider != null)
            {
                mainMusicSlider.value = -20f;
            }

            if (ambienceSlider != null)
            {
                ambienceSlider.value = -10f;
            }

            if (sfxSlider != null)
            {
                sfxSlider.value = -30f;
            }
        }

        /// <summary>
        /// Sets the master mixer volume to a slider's float value
        /// </summary>
        public void SetMasterVolumeBySlider()
        {
            if (masterSlider != null)
            {
                mixer.SetFloat("MasterVolume", masterSlider.value);
            }

        }

        /// <summary>
        /// Sets the main background music's volume to a slider's float value
        /// </summary>
        public void SetMainMusicVolumeBySlider()
        {
            if (mainMusicSlider != null)
            {
                mixer.SetFloat("MainMusicVolume", mainMusicSlider.value);
            }

        }

        /// <summary>
        /// Sets the main background music's volume to a float value
        /// </summary>
        public void SetMainMusicVolume(float volume)
        {
            mixer.SetFloat("MainMusicVolume", volume);
        }

        /// <summary>
        /// Sets the SFX volume to a slider's float value
        /// </summary>
        public void SetSFXVolumeBySlider()
        {
            if (sfxSlider != null)
            {
                mixer.SetFloat("SFXVolume", sfxSlider.value);
            }

        }

        /// <summary>
        /// Sets the ambience music's volume to a slider's float value
        /// </summary>
        public void SetAmbientVolumeBySlider()
        {
            if (ambienceSlider != null)
            {
                mixer.SetFloat("AmbienceVolume", ambienceSlider.value);
            }

        }

        /// <summary>
        /// Sets the ambience music's volume to a float value
        /// </summary>
        public void SetAmbientVolume(float volume)
        {
            mixer.SetFloat("AmbienceVolume", volume);
        }
        /// <summary>
        /// Saves Audio Configuration Settings to the database's Player Settings
        /// </summary>
        public void SaveAudioSettings()
        {
            float masterVol, mainMusicVol, ambienceVol, sfxVol;
            mixer.GetFloat("MasterVolume", out masterVol);
            mixer.GetFloat("MainMusicVolume", out mainMusicVol);
            mixer.GetFloat("AmbienceVolume", out ambienceVol);
            mixer.GetFloat("SFXVolume", out sfxVol);

            Video_Config.playerSettings.MasterVolume = Convert.ToInt32(masterVol);
            Video_Config.playerSettings.MainMusicVolume = Convert.ToInt32(mainMusicVol);
            Video_Config.playerSettings.AmbienceVolume = Convert.ToInt32(ambienceVol);
            Video_Config.playerSettings.SFXVolume = Convert.ToInt32(sfxVol);

        }


        /// <summary>
        /// Loads Audio Configuration Settings from the database's Player Settings
        /// </summary>
        public void LoadAudioSettings()
        {
            //mixer.SetFloat("MasterVolume", Video_Config.playerSettings.MasterVolume);
            //mixer.SetFloat("MainMusicVolume", Video_Config.playerSettings.MainMusicVolume);
            //mixer.SetFloat("SFXVolume", Video_Config.playerSettings.SFXVolume);
            //mixer.SetFloat("AmbienceVolume", Video_Config.playerSettings.AmbienceVolume);

            //masterSlider.value = Video_Config.playerSettings.MasterVolume;
            //mainMusicSlider.value = Video_Config.playerSettings.MainMusicVolume;
            //sfxSlider.value = Video_Config.playerSettings.SFXVolume;
            //ambienceSlider.value = Video_Config.playerSettings.AmbienceVolume;
        }

        public void PlaySFX(AudioClip SFX)
        {
            sfxSound.PlayOneShot(SFX);
        }

    }
}

