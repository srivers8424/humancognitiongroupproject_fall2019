﻿/*  @author Sonya Rivers-Medina
 *  @date 12/2/2019
 *  @purpose Controls a panel of a slot machine with 3 sprite images
 */
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

namespace SonyaScripts
{
    public class SlotMachinePanel : MonoBehaviour
    {
        // public List<Image> imgList;
        public bool spinning;
        public List<InfiniteScroll> scrollers;

        public void Spin()
        {
            foreach (InfiniteScroll i in scrollers)
            {
                i.Spinner();
            }
        }
    }
}

