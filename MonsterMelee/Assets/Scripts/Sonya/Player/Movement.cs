﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SonyaScripts
{
    public class Movement : MonoBehaviour
    {
        private float speed = 8.5f;
        private float maxPositon_yTop = 2.13f;
        private float maxPosition_yBottom = -2f;
        private float maxPosition_xLeft = -8f;
        private float maxPosition_xRight = 8f;
        private float moveHorizontal = 0.0f;
        private float lerpTime_slide = 0.13f; //slide lerp time
        private float currentLerpTime = 0.0f;
        private bool isSliding = false;
        private float slideMoveDistance = 3f;
        private Vector2 slideStartPos;
        private Vector2 slideEndPos;

        private PlayerAnimManager playerAnim;

        private Vector3 movement;
        private Rigidbody2D rb2D;
        private bool isMoving = false;

        //Jump values
        private bool isGrounded = true;
        private float groundCheckRadius = 1.2f;
        public LayerMask groundLayer;
        public Transform groundCheck;
        public float jumpPower = 7f;

        //Strings to set trigger animations
        private string slashAttack = "slashAttack";
        private string throwAttack = "throwAttack";
        private string kickAttack = "kickAttack";
        private string slideAttack = "slideAttack";

        void Start()
        {
            rb2D = GetComponent<Rigidbody2D>();
            playerAnim = GetComponent<PlayerAnimManager>();

        }

        private void Update()
        {
            //Find if the physics overlap is occuring between the ground layer, if true, grounded = true
            //otherwise grounded = false
            isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
            playerAnim.SetIsGrounded(isGrounded);

            //Check if grounded
            if (isGrounded && Input.GetButtonDown("Jump"))
            {
                Jump();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                playerAnim.SetAttack(slashAttack);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                playerAnim.SetAttack(kickAttack);
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                playerAnim.SetAttack(throwAttack);
            }

            if(Input.GetKeyDown(KeyCode.Alpha0))
            {
                playerAnim.Die();
            }

            //Slide
            if (Input.GetKeyDown(KeyCode.X))
            {
                //Make sure the player isn't already sliding
                //and they're on the ground
                if(!isSliding && isGrounded)
                {
                    //Reset Lerp time
                    currentLerpTime = 0f;
                    Slide();
                }
            }

            if(isSliding)
            {
                currentLerpTime += Time.deltaTime;
                if (currentLerpTime > lerpTime_slide)
                {
                    currentLerpTime = lerpTime_slide;
                }
                float t = currentLerpTime / lerpTime_slide;
                t = t * t * (3f - 2f * t);

                if (currentLerpTime == lerpTime_slide)
                {
                    isSliding = false;
                }

                transform.position = Vector3.Lerp(slideStartPos, slideEndPos, t);
            }

        }

        void FixedUpdate()
        {
            //Store the current horizontal input in the float moveHorizontal.
            moveHorizontal = Input.GetAxisRaw("Horizontal");

            playerAnim.SetMoveSpeed(Mathf.Abs(moveHorizontal));
            rb2D.velocity = new Vector2(moveHorizontal * speed, rb2D.velocity.y);

          
            //Make sure the player transform stays within bounds of the screen
            CheckPositionBoundary();
        }

        /// <summary>
        /// Checks the current player position to ensure it's not out of bounds of the screen
        /// </summary>
        private void CheckPositionBoundary()
        {
            //Check the height boundary position for top/bottom
            if (transform.position.y > maxPositon_yTop)
            {
                transform.position = new Vector3(transform.position.x, maxPositon_yTop, transform.position.z);
            }
            else if (transform.position.y < maxPosition_yBottom)
            {
                transform.position = new Vector3(transform.position.x, maxPosition_yBottom, transform.position.z);
            }


            //Check the side boundary posititons for left/right
            if (transform.position.x < maxPosition_xLeft)
            {
                transform.position = new Vector3(maxPosition_xLeft, transform.position.y, transform.position.z);
            }
            else if (transform.position.x > maxPosition_xRight)
            {
                transform.position = new Vector3(maxPosition_xRight, transform.position.y, transform.position.z);
            }
        }


        private void Jump()
        {
            Debug.Log("Starting jump from Movement script");
            playerAnim.Jump();
            // rb2D.AddForce(transform.up * jumpPower, ForceMode2D.Impulse);
            rb2D.velocity = new Vector2(rb2D.velocity.x, 0f);
            rb2D.AddForce(new Vector2(0f, jumpPower), ForceMode2D.Impulse);
            isGrounded = false;
        }

        private void Slide()
        {
            playerAnim.SetAttack(slideAttack);
            isSliding = true;
            slideStartPos = transform.position;
            if(playerAnim.flipX)
            {
                slideEndPos = transform.position + (transform.right * -1f) * slideMoveDistance;
            }
            else
            {
                slideEndPos = transform.position + transform.right * slideMoveDistance;
            }
           
            //Vector2 slideForce = new Vector2(slideHorizontal * speed * 10f, 0f);
            //  Debug.Log("Slide force " + slideForce.ToString());
            //rb2D.AddForce(slideVelocity, ForceMode2D.Impulse);

        }
    }
}


