﻿/*  @author Sonya Rivers-Medina
 *  @date 11/14/2019
 *  @purpose Scriptable object to track Player stats during runtime
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "NewStats", menuName = "Player/Stats", order =1)] 
public class PlayerStats_SO : ScriptableObject
{

    #region Fields
    public bool setManually = false;
    public bool saveDataOnClose = false;

    public int maxHealth = 0;
    public int currentHealth = 0;
    public int maxWealth = 0;
    public int curretWealth = 0;

    public int baseDmg = 0;
    public int currentDmg = 0;

    public float baseArmor = 0f;
    public float currentArmor = 0f;

    public float currentEmcumbrance = 0f;

    public int charExperience = 0;
    public int charLevel = 0;
    #endregion

}
