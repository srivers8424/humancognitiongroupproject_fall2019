﻿
/*  @author Sonya Rivers-Medina
 *  @date 11/20/2019
 *  @purpose Helper class to create Scriptable Objects for Player Audio Presets
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewPlayerAudio", menuName = "Player/Audio", order = 1)]
/// <summary>
///  Helper class to create Scriptable Objects for Player Audio Presets
/// </summary>
public class PlayerAudio_SO : ScriptableObject
{
    //Movement Sounds
    public AudioClip runSound_LeftFoot;
    public AudioClip runSound_RightFoot;
    //Jump Sounds
    public AudioClip jumpSound;
    public AudioClip jumpLandingSound;
    //Attack Sounds
    public AudioClip attackSound_Slashing;
    public AudioClip attackSound_Slashing_HitEnemy;
    public AudioClip attackSound_Throwing;
    public AudioClip attackSound_Kicking;
    public AudioClip attackSound_Sliding;
    //Hurt Soundsx
    public AudioClip hurtSound_Hurt;
    public AudioClip hurtSound_FallingDown;
    public AudioClip hurtSound_Dying;
}
