﻿/*  @author Sonya Rivers-Medina
 *  @date 11/14/2019
 *  @purpose Script to save/load/update the Player's stats
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SonyaScripts
{
    public class PlayerStats : MonoBehaviour
    {
        public static PlayerStats instance;
        public PlayerStats_SO playerStats_SO;

        private void Awake()
        {
            if(instance == null)
            {
                instance = this;
            }
        }
    }
}

