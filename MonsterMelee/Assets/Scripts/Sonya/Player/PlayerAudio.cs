﻿
/*  @author Sonya Rivers-Medina
 *  @date 11/20/2019
 *  @purpose Player Audio plays SFX sounds channeled by Player Animation Manager
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SonyaScripts
{
    /// <summary>
    ///  Player Audio plays SFX sounds channeled by Player Animation Manager
    /// </summary>
    public class PlayerAudio : MonoBehaviour
    {
        public static PlayerAudio instance;
        public PlayerAnimManager playerAnimManager;
        private float vol = 5f;
        private AudioSource source;
        public PlayerAudio_SO playerAudio;
        


        private void Awake()
        {
            if(instance == null)
            {
                instance = this;
            }
            source = GetComponent<AudioSource>();
        }

        private void OnEnable()
        {
            //Movement Animations
            playerAnimManager.OnPlayerRun += PlayRunSound;

            //Jump Sound Events
            playerAnimManager.OnPlayerJump += PlayJumpSound;
            playerAnimManager.OnPlayerGrounded += PlayJumpLandingSound;

            //Attack Sound Events
            playerAnimManager.OnPlayerSlashAttack += PlaySlashAttackSound;
            playerAnimManager.OnPlayerThrowAttack += PlayThrowingAttackSound;
            playerAnimManager.OnPlayerKickAttack += PlayKickingAttackSound;
            playerAnimManager.OnPlayerSlideAttack += PlaySlidingAttackSound;

            //Hurt Sound Events
            playerAnimManager.OnPlayerHurt += PlayHurtSound_Hurt;
            playerAnimManager.OnPlayerFallDown += PlayHurtSound_FallingDown;
            playerAnimManager.OnPlayerDying += PlayHurtSound_Dying;
        }

        private void OnDisable()
        {
            //Movement Animations
            playerAnimManager.OnPlayerRun -= PlayRunSound;

            //Jump Sound Events
            playerAnimManager.OnPlayerJump -= PlayJumpSound;
            playerAnimManager.OnPlayerGrounded -= PlayJumpLandingSound;

            //Attack Sound Events
            playerAnimManager.OnPlayerSlashAttack -= PlaySlashAttackSound;
            playerAnimManager.OnPlayerThrowAttack -= PlayThrowingAttackSound;
            playerAnimManager.OnPlayerKickAttack -= PlayKickingAttackSound;
            playerAnimManager.OnPlayerSlideAttack -= PlaySlidingAttackSound;

            //Hurt Sound Events
            playerAnimManager.OnPlayerHurt -= PlayHurtSound_Hurt;
            playerAnimManager.OnPlayerFallDown -= PlayHurtSound_FallingDown;
            playerAnimManager.OnPlayerDying -= PlayHurtSound_Dying;
        }


       
        #region Movement Sounds
        private void PlayRunSound()
        {
            //Run sound here
        }
        #endregion

        #region Jump Sounds
        private void PlayJumpSound()
        {
            source.PlayOneShot(playerAudio.jumpSound, vol);
        }

        private void PlayJumpLandingSound()
        {
            source.PlayOneShot(playerAudio.jumpLandingSound, vol);
        }
        #endregion

        #region Attack Sounds
        private void PlaySlashAttackSound()
        {
            source.PlayOneShot(playerAudio.attackSound_Slashing, vol);
        }

        public void PlaySlashAttackSound_HitEnemy()
        {
            source.PlayOneShot(playerAudio.attackSound_Slashing_HitEnemy, vol);
        }

        private void PlayThrowingAttackSound()
        {
            source.PlayOneShot(playerAudio.attackSound_Throwing, vol);
        }

        private void PlayKickingAttackSound()
        {
            source.PlayOneShot(playerAudio.attackSound_Kicking, vol);
        }

        private void PlaySlidingAttackSound()
        {
            source.PlayOneShot(playerAudio.attackSound_Sliding, vol);
        }
        #endregion

        #region Player Hurt Sounds
        private void PlayHurtSound_Hurt()
        {
            source.PlayOneShot(playerAudio.hurtSound_Hurt, vol);
        }
        private void PlayHurtSound_FallingDown()
        {
            source.PlayOneShot(playerAudio.hurtSound_FallingDown, vol);
        }
        private void PlayHurtSound_Dying()
        {
            //Play death cue music
            source.PlayOneShot(playerAudio.hurtSound_Dying, vol);
        }

        #endregion
    }
}
