﻿
/*  @author Sonya Rivers-Medina
 *  @date 11/120/2019
 *  @purpose Manages the Player's Animations based on player input and game state
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Animations;

namespace SonyaScripts
{
    /// <summary>
    /// Manages the Player's Animations based on player input and game state
    /// </summary>
    public class PlayerAnimManager : MonoBehaviour
    {
        public static PlayerAnimManager instance;
        public GameObject weapon;
        private Animator anim;
        public bool flipX = false;
        private float moveSpeed = 0.0f;
        private float blinkTimer = 0.0f;
       


        #region Animation Trigger Strings
        //Strings to set trigger animations
        private string slashAttack = "slashAttack";
        private string throwAttack = "throwAttack";
        private string kickAttack = "kickAttack";
        private string slideAttack = "slideAttack";
        #endregion


        #region Animation Events
        //Move Animations
        public delegate void PlayerAnim_Move();
        public PlayerAnim_Move OnPlayerRun;
        //Jump Animation
        public delegate void PlayerAnim_Jump();
        public PlayerAnim_Jump OnPlayerJump;
        public PlayerAnim_Jump OnPlayerGrounded;
        //Slash Attack Animation
        public delegate void PlayerAnim_SlashAttack();
        public PlayerAnim_SlashAttack OnPlayerSlashAttack;
        //Throw Attack Animation
        public delegate void PlayerAnim_ThrowAttack();
        public PlayerAnim_ThrowAttack OnPlayerThrowAttack;
        //Kick Attack Animation
        public delegate void PlayerAnim_KickAttack();
        public PlayerAnim_KickAttack OnPlayerKickAttack;
        //Slide Attack Animation
        public delegate void PlayerAnim_SlideAttack();
        public PlayerAnim_SlideAttack OnPlayerSlideAttack;
        //Player Death Animation
        public delegate void PlayerAnim_PlayerHurt();
        public PlayerAnim_PlayerHurt OnPlayerHurt;
        public PlayerAnim_PlayerHurt OnPlayerFallDown;
        public PlayerAnim_PlayerHurt OnPlayerDying;
        #endregion

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        private void Start()
        {
            anim = GetComponent<Animator>();

        }

        private void Update()
        {
            float h = Input.GetAxis("Horizontal");

            if (h > 0 && flipX)
            {
                flipX = false;
                FlipSprite();
            }
            else if (h < 0 && !flipX)
            {
                flipX = true;
                FlipSprite();
            }

            blinkTimer += Time.deltaTime;
            if (blinkTimer >= 5.0f && moveSpeed <= 0.1f)
            {
                PlayIdleBlink();
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                PlayIdleBlink();
            }
        }


        #region Movement Animation

        private void PlayIdleBlink()
        {
            anim.SetTrigger("blink");
            blinkTimer = 0.0f;
        }

        private void FlipSprite()
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }

        /// <summary>
        /// Sets a move speed float value for the Animator
        /// </summary>
        /// <param name="speed"></param>
        public void SetMoveSpeed(float speed)
        {
            moveSpeed = speed;
            anim.SetFloat("moveSpeed", speed);
        }

        public void Run()
        {
            OnPlayerRun();
        }

        public void Jump()
        {
            StartCoroutine(StartJump());
        }

        public void SetIsGrounded(bool grounded)
        {
            anim.SetBool("isGrounded", grounded);

        }
        private IEnumerator StartJump()
        {
            SetIsGrounded(false);
            anim.SetTrigger("jump");
            OnPlayerJump();
            yield return new WaitForSeconds(0.25f); //Time of jump animation 0.25 seconds
        }
        #endregion

        #region Attack Animations
        public void SetAttack(string attackName)
        {
            switch(attackName)
            {
                case "slashAttack":
                    SlashAttack();
                    break;
                case "throwAttack":
                    ThrowAttack();
                    break;
                case "kickAttack":
                    KickAttack();
                    break;
                case "slideAttack":
                    SlideAttack();
                    break;
            }
        }

        private  void SlashAttack()
        {
            anim.SetTrigger(slashAttack);
            OnPlayerSlashAttack();

            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.zero);
          
            if (hit.collider != null)
            {
                Debug.Log("hit " + hit.ToString()); 
                if (hit.collider.tag == "Enemy")
                {
                    // Calculate the distance from the surface and the "error" relative
                    // to the floating height.
                    float distance = hit.point.x - transform.position.x;
                    if (distance <= 3.0f)
                    {
                        // PlayerAudio.instance.PlaySlashAttackSound_HitEnemy();
                        Debug.Log("Enemy hit! at " + distance + " distance");
                    }
                }

            }
        }

        public void ThrowAttack()
        {
            anim.SetTrigger(throwAttack);
            OnPlayerThrowAttack();
        }

        public void KickAttack()
        {
            anim.SetTrigger(kickAttack);
            OnPlayerKickAttack();
        }

        public void SlideAttack()
        {
            anim.SetTrigger(slideAttack);
            OnPlayerSlideAttack();
        }

        #endregion

        public void Die()
        {
            anim.SetTrigger("playerHurt");
            anim.SetTrigger("playerDeath");
        }
    }
}
